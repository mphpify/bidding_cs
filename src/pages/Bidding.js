import React from 'react';
import { getOr, orderBy } from 'lodash/fp'

import { Paper, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Loader from 'react-loader-spinner'

import BidInput from '../components/BidInput';
import BidsList from '../components/BidsList';
import AlertDialog from '../components/Dialogs/AlertDialog';

import { getData } from '../services/bid'
import { getUser } from '../services/user'

const styles = () => ({
  paperRoot: {
    margin: '0 auto',
    width: '50%',
    padding: 10,
  },
});

class Bidding extends React.Component {
  state = {
    bids: [],
    user: null,
    error: [],
    loading: []
  }

  componentDidMount = () => {
    this.getUser()
    this.getBids()
  }

  /**
   * Get bids, sort it by amount, asc 
   */
  getBids = async () => {
    try {
      this.setState((state) => ({
        loading: [...state.loading, 'bids']
      }))

      const bids = await getData()

      this.setState((state) => ({
        bids: orderBy(['amount'], ['asc'], getOr([], `data`, bids)),
        loading: state.loading.filter(l => l !== 'bids'),
      }))
    } catch (e) {
      this.setState((state) => ({
        loading: state.loading.filter(l => l !== 'bids'),
        error: [...state.error, 'An error occured while getting data']
      }))
    }
  }

  /**
   * Get logged in user data
   */
  getUser = async () => {
    try {
      this.setState((state) => ({
        loading: [...state.loading, 'user']
      }))

      const user = await getUser();

      this.setState((state) => ({
        user,
        loading: state.loading.filter(l => l !== 'user'),
      }))
    } catch (e) {
      this.setState((state) => ({
        loading: state.loading.filter(l => l !== 'user'),
        error: [...state.error, 'An error occured while getting user data']
      }))
    }

  }

  /**
   * Add bid for the user
   * @param {*} amount 
   */
  addBid = (amount) => {
    // User shouldn't be able to enter value lower or equal to zero
    if (amount <= 0) {
      this.setState(state => ({
        error: [...state.error, "Bid's amount must be greater than zero."]
      }))
      return false
    }

    if (!this.hasBid(this.getCurrentUsersId())) {
      this.setState((state) => ({
        ...state,
        bids: orderBy(['amount'], ['asc'], [
          ...state.bids,
          {
            id: 463579,
            amount,
            created_at: new Date().toString(),
            status: 'placed',
            driver: state.user,
          }
        ])
      }));
    } else {
      this.setState((state) => ({
        ...state,
        bids: orderBy(['amount'], ['asc'], state.bids.map(bid => {
          if (bid.driver.id === state.user.id && bid.amount !== amount) {
            bid.amount = amount
          }
          return bid
        }))
      }));
    }
  }

  /**
   * Remove's bid
   * @param {*} bidId 
   */
  removeBid = (bidId) => {
    this.setState((state) => ({
      ...state,
      bids: orderBy(['amount'], ['asc'], state.bids.filter(bid => {
        return bid.id !== bidId
      }))
    }));
  }

  /**
   *  Return user's bid
   * @param {*} userId 
   */
  getUsersBid = (userId) => {
    return this.state.bids.find((bid) => {
      return bid.driver.id === userId
    })
  }

  /**
   * Check if user has bid
   * @param {*} userId 
   */
  hasBid = (userId) => {
    return this.getUsersBid(userId) !== undefined
  }

  /**
   * Get logged in user's id
   */
  getCurrentUsersId = () => {
    return this.state.user ? this.state.user.id : 0
  }

  /**
   * Closes Alert dialog
   */
  closeAlertDialog = () => {
    this.setState({ error: [] })
  }

  render() {
    const { classes } = this.props
    return this.state.loading.length > 0 ? <Loader></Loader> : <Paper classes={{ root: classes.paperRoot }}>
      <AlertDialog open={this.state.error.length > 0} onClose={this.closeAlertDialog}>{this.state.error.join('<br/>')}</AlertDialog>
      <Typography variant="subtitle1" align={"left"}> Existing bids: </Typography>
      <BidsList bids={this.state.bids} user={this.state.user} removeBid={this.removeBid}></BidsList>
      <BidInput label={"Your bid:"} onClick={this.addBid}>{this.hasBid(this.getCurrentUsersId()) ? 'Update Bid' : 'Place Bid'}</BidInput>
    </Paper>
  }
}

export default withStyles(styles)(Bidding)