import { getOr, sample } from 'lodash/fp'
import axios from 'axios'

/**
 * Get random logged in user
 */
export const getUser = async () => {
  const jsonData = await axios.get('https://api.jsonbin.io/b/5f563626993a2e110d403821')
  const bid = sample(getOr([], `data.data.bids.data`, jsonData));
  return bid.driver
}
