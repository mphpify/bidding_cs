import { getOr } from 'lodash/fp'
import axios from 'axios'

/**
 * Simulates getting the data from API
 * @param {string} field 
 */
export const getData = async () => {
  const jsonData = await axios.get('https://api.jsonbin.io/b/5f563626993a2e110d403821')
  return getOr(null, `data.data.bids`, jsonData)
}
