import React from 'react';
import './App.css';

import Bidding from './pages/Bidding';


function App() {
  return (
    <div className="App">
      <Bidding></Bidding>
    </div>
  );
}

export default App;
