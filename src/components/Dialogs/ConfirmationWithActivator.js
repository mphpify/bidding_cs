import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  paper: {
    position: 'relative',
    top: `40%`,
    margin: '0 auto',
    width: theme.spacing(50),
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    paddingTop: theme.spacing(2.5),
  },
  dialogHeader: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(1.25),
    fontWeight: 'bolder',
  },
  dialogButtonsWrapper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  activator: {
    cursor: 'pointer',
  },
})


class ConfirmationWithActivaor extends React.Component {
  state = {
    open: false,
  }

  /**
   * Handle confirmation action
   */
  handleYes = () => {
    this.setState({ open: false })
    this.props.onClick(true, this.props.id)
  }

  /**
   * Handle reject action
   */
  handleNo = () => {
    this.setState({ open: false })
    this.props.onClick(false)
  }

  /**
   * Handle activator action, open dialog
   */
  activatorOnClick = () => {
    if (this.props.disabled !== true) {
      this.setState({ open: true })
    }
  }

  render() {
    const { classes } = this.props

    return (
      <div>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={Boolean(this.state.open)}
          onClose={this.handleNo}>
          <div className={classes.paper}>
            <div className={classes.dialogHeader}>
              {this.props.title ? this.props.title : 'Confirmation'}
            </div>
            {this.props.children}
            <div className={classes.dialogButtonsWrapper}>
              <Button
                style={{ marginRight: 10 }}
                color="primary"
                variant="contained"
                onClick={this.handleYes}
                disabled={
                  this.props.confirmButtonEnabled !== undefined &&
                  !this.props.confirmButtonEnabled
                }>
                {this.props.textYes ? this.props.textYes : 'Yes'}
              </Button>
              <Button
                color="secondary"
                variant="contained"
                onClick={this.handleNo}>
                {this.props.textNo ? this.props.textNo : 'No'}
              </Button>
            </div>
          </div>
        </Modal>
        <div
          onClick={this.activatorOnClick}
          className={this.props.classes.activator}>
          {this.props.activator || null}
        </div>
      </div>
    )
  }
}

ConfirmationWithActivaor.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  title: PropTypes.string,
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.any,
  open: PropTypes.any,
  variant: PropTypes.string,
  confirmButtonEnabled: PropTypes.bool,
  activator: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
}

export default withStyles(styles)(ConfirmationWithActivaor)