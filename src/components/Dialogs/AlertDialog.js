import React from 'react';
import PropTypes from 'prop-types'
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';

class AlertDialog extends React.PureComponent {

  render() {
    return <Dialog
      open={this.props.open}
      onClose={this.props.onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Error"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {this.props.children}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={this.props.onClose} color="primary">
          {this.props.buttonText ? this.props.buttonText : 'OK'}
        </Button>
      </DialogActions>
    </Dialog>
  }
}


AlertDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  buttonText: PropTypes.string,
}

export default AlertDialog