import React from 'react';
import PropTypes from 'prop-types'
import { ListItem, List, withStyles } from '@material-ui/core';

import ConfirmationWithActivator from './Dialogs/ConfirmationWithActivator';


const styles = (theme) => ({
  removeBtn: {
    color: theme.palette.secondary.main,
    paddingLeft: "10px"
  }
})

/**
 * Displays list of bidds
 */
class BidsList extends React.PureComponent {

  /**
   * Removes bid
   * 
   * @param {boolean} result 
   * @param {number} bidId 
   */
  removeBid = (result, bidId) => {
    if (result === true) {
      this.props.removeBid(bidId)
    }
  }

  render() {
    return <List>
      {this.props.bids.map(bid => {
        return <ListItem selected={bid.driver.id === this.props.user.id} key={bid.id}>{bid.driver.display_name}: {`$${bid.amount}`}
          {bid.driver.id === this.props.user.id ? <ConfirmationWithActivator id={bid.id} onClick={this.removeBid} activator={<div className={this.props.classes.removeBtn}>X</div>}>Do you want to remove Your bid?</ConfirmationWithActivator> : null}
        </ListItem>
      })}
    </List>
  }
}


const driverPropType = PropTypes.shape({
  id: PropTypes.number,
  display_name: PropTypes.string,
  first_name: PropTypes.string,
  full_name: PropTypes.string,
  profile_slug: PropTypes.string,
})

const bidPropType = PropTypes.shape({
  id: PropTypes.number,
  amount: PropTypes.number,
  status: PropTypes.string,
  created_at: PropTypes.string,
  driver: driverPropType
})

BidsList.propTypes = {
  removeBid: PropTypes.func,
  bids: PropTypes.arrayOf(bidPropType),
  user: driverPropType,
}


export default withStyles(styles)(BidsList)