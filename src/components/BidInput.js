import React from 'react';
import PropTypes from 'prop-types'
import { Button, TextField, withStyles } from '@material-ui/core';


const styles = () => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'

  },
  inputRoot: {
    marginRight: '10px'
  },
  buttonRoot: {
    height: '40px'
  }
});

class BidInput extends React.Component {
  state = {
    value: 0,
    error: ''
  }

  /**
   * 
   * @param {Updates field's value}
   */
  updateValue = (e) => {
    const numbersOnly = new RegExp(/^\d+$/)
    const value = e.target.value

    if (value.length <= 0) {
      this.setState({ error: 'Please enter a value.', value })
      return false;
    } else if (!numbersOnly.test(value)) {
      this.setState({ error: 'Only numbers are allowed', value })
      return false;
    } else {
      this.setState({
        error: ''
      })
    }

    this.setState({
      value: parseInt(value),
    })
  }

  /** 
   * Sets value
   */
  setValue = () => {
    if (!this.hasError()) {
      this.props.onClick(this.state.value)
    }
  }

  /**
   * Checks if there is an error
   */
  hasError = () => {
    return this.state.error.length > 0
  }

  render() {
    const { classes } = this.props

    return <div className={classes.wrapper}>
      <TextField
        classes={{ root: classes.inputRoot }}
        error={this.hasError()}
        helperText={this.state.error}
        value={this.state.value}
        onChange={this.updateValue}
        label={this.props.label || ''}
      />
      <Button variant="outlined" color="primary" onClick={this.setValue} disabled={this.hasError()} classes={{ root: classes.buttonRoot }}>{this.props.children}</Button>
    </div>
  }
}

BidInput.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
}

export default withStyles(styles)(BidInput)